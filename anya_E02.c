#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>

char rot13(char x){
    return ((x - 'a' + 13)%26 + 'a');
}

char atbash(char x){
    return (25 - (x-'A') + 'A');
}

char * crypt(char * path){
    char* rpath;
    strcpy(rpath, path);
    char* found = strstr(path, "Animeku_");
    if(found != NULL){
        int pos = found + 8 - &path[0];
        struct stat fs;
        stat(path, &fs);
        int end = strlen(path);
        if(S_ISREG(fs.st_mode)){
            for(int i=strlen(path)-1; i>=0; i--){
                if(path[i] == '.'){
                    end = i;
                    break;
                }
            }
            if(end < pos) end = strlen(path);
        }
        for(int i=pos; i<end; i++){
            if(path[i] >= 'a' && path[i] <= 'z') rpath[i] = rot13(path[i]);
            else if(path[i] >= 'A' && path[i] <= 'Z') rpath[i] = atbash(path[i]);
        }

        return rpath;
    }
    return path;
}

static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    res = lstat(path, stbuf);

    if (res == -1) return -errno;
    return 0;
}



static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(path);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if(filler(buf, de->d_name, &st, 0)) break;
    }
    closedir(dp);
    return 0;
}



static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    int fd;
    int res;
    (void) fi;

    fd = open(path, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}



static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
};



int  main(int  argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}