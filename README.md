# Sisop soal shift modul 4 kelompok E02 2022
Anggota:
1. Haniif Ahmad Jauhari | 5025201224
2. Made Rianja Richo Dainino | 5025201236
3. Dicky Indra Kuncahyo | 5025201250

## Soal 1
Dikerjakan oleh Haniif Ahmad Jauhari | 5025201224

Pada problem ini, untuk mengenkripi sebuah karakter huruf kecil dalam enkripsi ROT13, digunakan fungsi berikut.

    char rot13(char x){
        return ((x - 'a' + 13)%26 + 'a');
    }

Kemudian, untuk mengenkripis sebuah karakter huruf besar dalam enkripsi atbash cipher, digunakan fungsi berikut.

    char atbash(char x){
        return (25 - (x-'A') + 'A');
    }

Kemudian fungsi `crypt` berikut ini berfungsi untuk menerima nama file dalam bentuk direktori dan mengembalikan nama file hasil enkripsi (bila perlu di enkripsi).

    char * crypt(char * path){
        char* rpath;
        strcpy(rpath, path);
        char* found = strstr(path, "Animeku_");
        if(found != NULL){
            int pos = found + 8 - &path[0];
            struct stat fs;
            stat(path, &fs);
            int end = strlen(path);
            if(S_ISREG(fs.st_mode)){
                for(int i=strlen(path)-1; i>=0; i--){
                    if(path[i] == '.'){
                        end = i;
                        break;
                    }
                }
                if(end < pos) end = strlen(path);
            }
            for(int i=pos; i<end; i++){
                if(path[i] >= 'a' && path[i] <= 'z') rpath[i] = rot13(path[i]);
                else if(path[i] >= 'A' && path[i] <= 'Z') rpath[i] = atbash(path[i]);
            }
    
            return rpath;
        }
        return path;
    }


## Soal 2
Dikerjakan oleh Made Rianja Richo Dainino | 5025201236

## Soal 3
Dikerjakan oleh Dicky Indra Kuncahyo | 5025201250
